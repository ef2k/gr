import React from 'react';
import PropTypes from 'prop-types';

import {
  Table,
} from 'reactstrap';

const EmptyTable = ({ rows, cols }) => (
  <Table bordered responsive>
    <thead>
      <tr>
        {Array(cols).fill().map((th, idx) => <th key={idx}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th> )}
      </tr>
    </thead>
    <tbody>
      {Array(rows).fill().map((tr,idx) => (
        <tr key={idx}>
          {Array(cols).fill().map(td => <td key={idx} />)}
        </tr>
      ))}
    </tbody>
  </Table>
);

const ReportTable = (props) => {
  const {
    defaultRows,
    defaultCols,
  } = props;
  return (
    <EmptyTable rows={defaultRows} cols={defaultCols} />
  );
};

ReportTable.propTypes = {
  defaultRows: PropTypes.number,
  defaultCols: PropTypes.number,
};

ReportTable.defaultProps = {
  defaultRows: 25,
  defaultCols: 8,
};

export default ReportTable;
