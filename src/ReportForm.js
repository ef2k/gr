import React, { Component } from 'react';
import {
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from 'reactstrap';

export const TIMEZONES = [
  { name: 'US/Eastern', value: 0 },
  { name: 'US/Mountain', value: 1 },
  { name: 'US/Pacific', value: 2 },
  { name: 'US/Hawaii', value: 3 },
];

class ReportForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneNumber: '',
      jiraTicket: '',
      timezone: 0,
    };
  }

  handleChange = (e, fieldName) => {
    const value = e.target.value;
    this.setState({
      [fieldName]: value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    window.alert(JSON.stringify(this.state));
  };

  render() {
    const {
      phoneNumber,
      jiraTicket,
      timezone,
    } = this.state;
    const {
      handleSubmit,
      handleChange,
    } = this;
    return (
      <Form className="ReportForm" onSubmit={handleSubmit}>
        <FormGroup>
          <Label for="phone-number">Phone Number</Label>
          <Input
            id="phone-number"
            placeholder="Phone number"
            value={phoneNumber}
            onChange={(e) => handleChange(e, 'phoneNumber')}
            required
          />
        </FormGroup>
        <FormGroup>
          <Label for="jira-ticket">Jira Ticket</Label>
          <Input
            id="jira-ticket"
            placeholder="Jira Ticket"
            value={jiraTicket}
            onChange={(e) => handleChange(e, 'jiraTicket')}
            required
          />
        </FormGroup>
        <FormGroup>
          <Label for="timezone">Timezone</Label>
          <Input
            id="timezone"
            type="select"
            placeholder="Timezone"
            value={timezone}
            onChange={(e) => handleChange(e, 'timezone')}
          >
            {TIMEZONES.map(tz => <option value={tz.value}>{tz.name}</option>)}
          </Input>
        </FormGroup>
        <FormGroup>
          <Button color="primary">Submit</Button>
        </FormGroup>
      </Form>
    );
  }
}

export default ReportForm;
