import React, { Component } from 'react';
import {
  Container,
  Row, 
  Col,
} from 'reactstrap';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';


import ReportForm from './ReportForm';
import ReportTable from './ReportTable';
import CSVExporter from './CSVExporter';

class App extends Component {
  render() {
    return (
      <Container className="App">
        <Row>
          <Col md={3}>
            <ReportForm />
          </Col>
          <Col md={6}>
            <ReportTable />
          </Col>
          <Col md={3}>
            <CSVExporter />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;
