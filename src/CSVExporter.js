import React from 'react';
import PropTypes from 'prop-types';

export const toCSV = jsonData => jsonData;

const CSVExporter = ({ data, disabled }) => (
  <a
    id="csv-download"
    href={`data:text/csv;charset=UTF-8, ${encodeURIComponent(toCSV(data))}`}
    className={`btn btn-outline-primary ${disabled? 'disabled' : ''}`}
    role="button"
  >
    Download CSV
  </a>
);

CSVExporter.propTypes = {
  data: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
};

CSVExporter.defaultProps = {
  disabled: true,
};

export default CSVExporter;
